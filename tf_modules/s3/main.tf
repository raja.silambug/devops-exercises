resource "aws_s3_bucket" "thisbucket" {
  bucket = "${var.bucket_name}"
  acl    = "${var.acl}"

  tags = {
    Terraform = "true"
  }
}

resource "aws_s3_bucket_object" "this_bucket_object" {
  count  = "${length(var.objects_to_upload)}"
  key    = "${element(var.objects_to_upload, count.index)}"
  bucket = "${aws_s3_bucket.thisbucket.bucket}"
  source = "${element(var.objects_to_upload, count.index)}"
  etag   = "${md5(file(element(var.objects_to_upload, count.index)))}"

  tags = {
    Terraform = "true"
  }
}
