resource "aws_security_group" "eks-worker" {
  name        = "${var.name}-worker"
  description = "Security group for all nodes in the cluster"
  vpc_id      = "${var.vpc_id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${var.tags}"
}

resource "aws_security_group_rule" "eks-worker-ingress-self" {
  description              = "Allow node to communicate with each other"
  from_port                = 0
  protocol                 = "-1"
  security_group_id        = "${aws_security_group.eks-worker.id}"
  source_security_group_id = "${aws_security_group.eks-worker.id}"
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "eks-worker-ingress-cluster" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 1025
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.eks-worker.id}"
  source_security_group_id = "${aws_security_group.eks-cluster.id}"
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_launch_configuration" "eks-worker" {
  iam_instance_profile = "${aws_iam_instance_profile.eks-worker.name}"
  image_id             = "${data.aws_ami.eks-worker.id}"
  instance_type        = "${var.worker_instance_type}"
  name_prefix          = "${var.name}-worker"
  security_groups      = ["${aws_security_group.eks-worker.id}"]
  user_data_base64     = "${base64encode(local.eks-worker-userdata)}"
  key_name             = "${var.key_name}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "eks-worker" {
  desired_capacity     = "${var.worker_desired_count}"
  launch_configuration = "${aws_launch_configuration.eks-worker.id}"
  max_size             = "${var.worker_max_count}"
  min_size             = "${var.worker_min_count}"
  name                 = "${var.name}-worker"
  vpc_zone_identifier  = ["${var.private_subnet_ids[0]}", "${var.private_subnet_ids[1]}"]

  tag {
    key                 = "Name"
    value               = "${var.name}-worker"
    propagate_at_launch = true
  }

  tag {
    key                 = "kubernetes.io/cluster/${var.name}"
    value               = "owned"
    propagate_at_launch = true
  }
}

resource "aws_efs_mount_target" "k8-efs" {
  count           = "${ var.efs_id != "" ? 1 : 0 }"
  file_system_id  = "${var.efs_id}"
  subnet_id       = "${var.private_subnet_ids[0]}"
  security_groups = ["${aws_security_group.eks-worker.id}"]
}
