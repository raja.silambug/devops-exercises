variable "name" {}

variable "vpc_id" {}

variable "my_ip" {}

variable "subnet_ids" {
  type  = "list"
}

variable "private_subnet_ids" {
  type = "list"
}

variable "tags" {
  type    = "map"
  default = {}
}

variable "trusted_role_arns" {
  type    = "list"
  default = []
}

variable "efs_id" {
  default = ""
}

variable "worker_desired_count" {}
variable "worker_instance_type" {}
variable "worker_max_count" {}
variable "worker_min_count" {}
variable "key_name" {}
