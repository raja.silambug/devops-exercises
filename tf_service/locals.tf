locals {
  env = "${var.workspace}"

  env_sshpubkey_file = {
    "dev"  = "${var.dev_sshpubkey_file}"
    "prod" = "${var.prod_sshpubkey_file}"
  }

  sshpubkey_file = "${var.sshpubkey_file}"

  common_tags = {
    "Terraform"   = "true"
    "Environment" = "${local.env}"
  }
}
