variable "access_key" {}
variable "secret_key" {}

variable "region" {}

variable "my_ip" {}

variable "network_state_s3_bucket" {}

variable "network_key" {}

variable "workspace" {}

variable "dev_sshpubkey_file" {}
variable "prod_sshpubkey_file" {}

variable "sshpubkey_file" {}

variable "files" {
    type    = "list"
    default = ["./files_to_upload/file1.jpg", "./files_to_upload/file2.jpg", "./files_to_upload/file3.jpg", "./files_to_upload/file4.jpg"]
}

variable "s3_bucket_name" {}