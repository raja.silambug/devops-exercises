
module "my_eks" {
  name   = "${local.env}-eks"
  source = "../tf_modules/eks"
  vpc_id               = "${data.terraform_remote_state.network.outputs.vpc_id}"
  subnet_ids           =  "${concat(data.terraform_remote_state.network.outputs.private_subnet_ids, data.terraform_remote_state.network.outputs.public_subnet_ids)}"
    #  subnet_ids         = ["${var.subnet_ids[0]}", "${var.subnet_ids[1]}", "${var.subnet_ids[2]}", "${var.subnet_ids[3]}"]
  private_subnet_ids   = "${data.terraform_remote_state.network.outputs.private_subnet_ids}"
  worker_instance_type = "m4.large"
  worker_desired_count = 2
  worker_max_count     = 4
  worker_min_count     = 1
  my_ip                = "${var.my_ip}"

  key_name = "${data.terraform_remote_state.network.outputs.ssh_key_name}"
}
