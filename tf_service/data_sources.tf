data "terraform_remote_state" "network" {
    backend = "s3"
    config = {
    bucket = "${var.network_state_s3_bucket}"
    key    = "${var.network_key}"
    region = "${var.region}"
  }
}
