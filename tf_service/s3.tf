resource "random_pet" "my-s3" {
  length    = "2"
  prefix    = "my-s3"
  separator = "-"
}

module "my-s3" {
  source      = "../tf_modules/s3"
  bucket_name = "${random_pet.my-s3.id}"
}

resource "aws_s3_bucket" "bucket1" {
  bucket = "${var.s3_bucket_name}"
  acl    = "private"

  versioning {
    enabled = true
  }
}


resource "aws_s3_bucket_object" "object" {
count = "${length(var.files)}"
  bucket = "${aws_s3_bucket.bucket1.id}"
  key    = "new_object_key-${count.index}"
  source = "${element(var.files, count.index)}"
  }