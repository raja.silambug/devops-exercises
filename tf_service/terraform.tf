provider "aws" {
  region = "ap-south-1"
}

terraform {
  backend "s3" {
    key     = "services_tfstate"
    region  = "ap-south-1"
    encrypt = true
  }
required_version = "~> 0.12"
}
