apt-get install cowsay -y
apt-get install htop -y
apt-get install tmux -y
apt-get install net-tools -y
echo '*               soft    nofile          2048' >> /etc/security/limits.conf
echo '*               hard    nofile          2097152' >> /etc/security/limits.conf
sed -i -e "s/\(net.ipv4.ip_forward=\).*/\10/" /etc/sysctl.conf
apt-get remove netcat -y
apt-get install nginx -y
