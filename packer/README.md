## Pre-requisite

Export `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` so that packer can use AWS resources to build the AMI.

```
#!/usr/bin/env bash

export AWS_ACCESS_KEY_ID="your-key-here"
export AWS_SECRET_ACCESS_KEY="your-sec-key-here"
```


## How to run

```
 packer build packer.json
```