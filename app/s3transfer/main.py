from flask import Flask,request, send_file
from werkzeug import secure_filename
import config as cp

app = Flask(__name__)
import boto3,os
import botocore
s3 = boto3.client(
   "s3",
   cp.aws_region,
   aws_access_key_id=cp.aws_access_key_id,
   aws_secret_access_key = cp.aws_secret_access_key
)
bucket_name = cp.bucket_name

@app.route('/')
def main():
	return 'This is an example'

@app.route('/upload/',methods = ['POST'])
def upload_file():
    print('test');
    if request.method =='POST':
        print('test23');
        print(request.files);
        file = request.files['file']
        if file:
            filename = secure_filename(file.filename)
            print(filename);
            s3.upload_fileobj(file,bucket_name,file.filename, ExtraArgs={"ACL": "public-read","ContentType": file.content_type})
    return 'Uploaded Successfully'
	
	
	
@app.route('/download/<fileName>')
def download_file(fileName):
	try:
		test = s3.download_file(bucket_name,fileName, fileName);
	except botocore.exceptions.ClientError as e:
		if e.response['Error']['Code'] == "404":
			print("The object does not exist.")
		else:
			raise
	#return send_file(fileName);
	return("test");
			
app.run('127.0.0.1', 8090)