
## Pre-requisite

- python 3 [installation document](https://realpython.com/installing-python/)
- flask [pip install flask]
- boto3 [pip install boto3]


## How to run
```
python main.py
```


### config.py

Four variables need to be passed for the script to work. Fill the values for the variables in 'config.py' file.

aws_access_key_id="your-key-here"
aws_secret_access_key = "your-sec-key-here"
aws_region = "region of s3 bucket"
bucket_name = "Bucket to which to upload or download from"

### How to check if the program is working

The APIs are exposed to port 8090. Below are the [CURL](https://curl.haxx.se/docs/manpage.html) commands to upload and download files to/from S3. 

upload:
```
curl -X POST -F file=@"C:\Users\mahendranp\Desktop\filename.txt" http://127.0.0.1:8090/upload/
```

download:
```
curl -X GET http://127.0.0.1:8090/download/filename.txt
```