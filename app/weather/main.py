from flask import Flask,request
import datetime
app = Flask(__name__)

@app.route('/')
def main():
	return 'This is an example'

data = [{'city':'london','date':'20190624','temperature':'23degree'},{'city':'london','date':'20190620','temperature':'22degree'},{'city':'london','date':'20190621','temperature':'18degree'},{'city':'london','date':'20190622','temperature':'20degree'},{'city':'london','date':'20190623','temperature':'23degree'},{'city':'DC','date':'20190624','temperature':'23degree'}]
@app.route('/weather/<string:city>/<date>')
@app.route('/weather/<string:city>/',defaults={'date':None})
def weather(city,date):
	weatherFor = '';
	listOfDetails = [];
	if date is None:
		today = datetime.date.today()
		weatherFor = today.strftime("%Y%m%d");
	else:
		weatherFor = date

	for i in data:
		if(i['city'] == city and i['date'] == weatherFor):
			listOfDetails.append(i)

	return(str(listOfDetails));

app.run('127.0.0.1', 9090)
