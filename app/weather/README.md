
## Pre-requisite

- python 3 [installation document](https://realpython.com/installing-python/)
- flask [pip install flask]


## How to run
```
python main.py
```

### How to check if the program is working

The APIs are exposed to port 9090. Below are the [CURL](https://curl.haxx.se/docs/manpage.html) commands to check the weather of any day.

```
curl http://127.0.0.1:9090/weather/DC/20190624
```

Explanation:

DC          --> DC is the city name (As of now only two cities are added to the programm DC and London)
20190624    --> Date in YYYYMMDD format