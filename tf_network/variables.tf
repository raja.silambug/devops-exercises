variable "access_key" {}
variable "secret_key" {}

variable "region" {
  default = "eu-west-1"
}

variable "dev_sshpubkey_file" {}
variable "prod_sshpubkey_file" {}

variable "workspace" {}

/*locals {
  env = "dev"
  sshpubkey_file = "C:/Users/Guest-1/Downloads/dev-ssh-pubkey.pem"
}

resource "local_file" "sshpubkey_file" {
    content     = "foo!"
    filename = "${path.module}/foo.bar"
}*/