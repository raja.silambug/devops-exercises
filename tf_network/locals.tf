locals {
  #env = "${terraform.workspace == "default" ? "dev" : terraform.workspace}"
  env = "${var.workspace}"

  vpc_cidrs = {
    "dev"  = "10.0.0.0/16"
    "prod" = "10.1.0.0/16"
  }

  private_subnets_map = {
    "dev"  = ["10.0.1.0/24", "10.0.2.0/24"]
    "prod" = ["10.1.1.0/24", "10.1.2.0/24"]
  }

  public_subnets_map = {
    "dev"  = ["10.0.10.0/24", "10.0.11.0/24"]
    "prod" = ["10.1.10.0/24", "10.1.11.0/24"]
  }

  database_subnets_map = {
    "dev"  = ["10.0.20.0/24", "10.0.21.0/24"]
    "prod" = ["10.1.20.0/24", "10.1.21.0/24"]
  }

  env_sshpubkey_file_map = {
    "dev"  = "${var.dev_sshpubkey_file}"
    "prod" = "${var.prod_sshpubkey_file}"
  }

  vpc_cidr         = "${local.vpc_cidrs[local.env]}"
  private_subnets  = "${local.private_subnets_map[local.env]}"
  public_subnets   = "${local.public_subnets_map[local.env]}"
  database_subnets = "${local.database_subnets_map[local.env]}"
  sshpubkey_file   = "${local.env_sshpubkey_file_map[local.env]}"

  common_tags = {
    "Terraform"   = "true"
    "Environment" = "${local.env}"
    "env"         = "${local.env}"
    "author"      = "raja silambu"
  }
}
