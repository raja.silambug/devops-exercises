output "vpc_id" {
  description = "The ID of the VPC"
  value       = "${module.vpc.vpc_id}"
}

output "vpc_cidr_block" {
  description = "The CIDR block of the VPC"
  value       = "${module.vpc.vpc_cidr_block}"
}

output "private_subnet_ids" {
  description = "Private Subnet IDs of the VPC"
  value       = "${module.vpc.private_subnets}"
}

output "public_subnet_ids" {
  description = "Public Subnet IDs of the VPC"
  value       = "${module.vpc.public_subnets}"
}

output "private_subnets" {
  description = "Private Subnets of the VPC"
  value       = "${local.private_subnets}"
}

output "public_subnets" {
  description = "Public Subnets of the VPC"
  value       = "${local.public_subnets}"
}

output "private_route_table_ids" {
  value = "${module.vpc.private_route_table_ids}"
}

output "public_route_table_ids" {
  value = "${module.vpc.public_route_table_ids}"
}

output "database_route_table_ids" {
  value = "${module.vpc.database_route_table_ids}"
}

output "route_table_ids" {
  value = ["${module.vpc.private_route_table_ids}",
    "${module.vpc.public_route_table_ids}",
    "${module.vpc.database_route_table_ids}",
  ]
}
