module "vpc" {
  name    = "${local.env}-vpc"
  source  = "terraform-aws-modules/vpc/aws"

  cidr = "${local.vpc_cidr}"

  azs = [
    "${data.aws_availability_zones.available.names[0]}",
    "${data.aws_availability_zones.available.names[1]}",
  ]

  private_subnets  = "${local.private_subnets}"
  public_subnets   = "${local.public_subnets}"
  database_subnets = "${local.database_subnets}"

  
  enable_dns_hostnames = true
  enable_dns_support   = true

  create_database_subnet_group = false

  tags = "${merge(local.common_tags, map("env", local.env))}"
  
}

resource "aws_security_group" "packer_build" {
  name        = "packer-build"
  description = "Security Group for Packer Builds"
  vpc_id      = "${module.vpc.vpc_id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 5986
    to_port     = 5986
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 5985
    to_port     = 5985
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "packer-build"
    Env  = "${local.env}"
  }
}
