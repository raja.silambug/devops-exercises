provider "aws" {
  region = "ap-south-1"
}

terraform {
  backend "s3" {
    key     = "network_tfstate"
    region  = "ap-south-1"
    encrypt = true
  }
required_version = "~> 0.12"
}

resource "aws_key_pair" "ssh_key" {
  key_name   = "${local.env}-ssh-pubkey"
  public_key = "${file(local.sshpubkey_file)}"
}
